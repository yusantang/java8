import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Optional;
class FirstJunit5Tests {
	@Test
	void testSomeLibraryMethod() {
		Library classUnderTest = new Library();
		assertTrue( classUnderTest.someLibraryMethod());
	}
	
	@Test
	void testSort() {
		List<String> names = new ArrayList<String>();
		names.add("tang");
		names.add("tong");
		
		Library classUnderTest = new Library();
		classUnderTest.sortDesc(names);
		assertEquals("[tong, tang]",names.toString());
		classUnderTest.sortAsc(names);
		assertEquals("[tang, tong]",names.toString());
	}
	
	@Test
	void testMathOperation() {
		Library classUnderTest = new Library();
		
		int result = classUnderTest.operate(1,1,(a,b) -> a+b);
		assertEquals(2, result);
		
		MathOperation sub = (a, b) -> a - b;
		assertEquals(1, classUnderTest.operate(3,2,sub));
		
		
	}
	
	@Test
	void testPredicate() {
		Library lib = new Library();
		List<Integer> list = new ArrayList<Integer>();
		list.add(2);
		list.add(4);
		list.add(5);
		assertEquals("[2, 4]",lib.filter(list,n -> n % 2 == 0).toString());
		
		assertEquals("[4, 5]",lib.filter(list,n -> n > 2).toString());
	}
	
	@Test
	void testDefaultInterface() {
		Library lib = new Library();
		assertEquals("ArrayListLibrary",lib.name());
	}
	
	@Test
	void testStreamFilter() {
		Library lib = new Library();
		assertEquals("[2, 4]",lib.filterBySteam(Arrays.asList(2,4,5),n -> n% 2 == 0).toString());
	}
	
	@Test
	void testOptionalSum() {
		Library lib = new Library();
		Optional<Integer> a = Optional.ofNullable(null);
		Optional<Integer> b = Optional.of(new Integer(10));
		assertEquals(new Integer(10), lib.sum(b,a));
	}
}