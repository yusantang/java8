import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

import java.util.function.Predicate;

import java.util.stream.Collectors;

import java.util.Optional;
public class Library implements Array, MyArray {
	 
	public boolean someLibraryMethod() {
		return true;
	}
	
	public void sortAsc(List<String> names) {
		Collections.sort(names, (s1,s2) -> s1.compareTo(s2));
	}
	
	public void sortDesc(List<String> names) {
		Collections.sort(names, (s1,s2) -> s2.compareTo(s1));
	}
	
	public int operate(int a,int b,MathOperation mathOpration) {
		return mathOpration.operate(a, b);
	}
	
	public List<Integer> filter(List<Integer> list,Predicate<Integer> pre) {
		List<Integer> resultList = new ArrayList<Integer>();
		for(Integer n: list) {
			if(pre.test(n)) {
				resultList.add(n);
			}
		}
		return resultList;
	}
	
	public String name() {
		return Array.super.name() + MyArray.super.name() + "Library";
	}
	
	public List<Integer> filterBySteam(List<Integer> list, Predicate<Integer> pre) {
		return list.stream().filter(pre).collect(Collectors.toList());
	}
	
	public Integer sum(Optional<Integer> a,Optional<Integer> b) {
		Integer first = a.orElse(new Integer(0));
		Integer second = b.orElse(new Integer(0));
		
		return first + second;
	}
}